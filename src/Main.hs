{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}

module Main (main) where

import Data.Char ( isSpace
                 , toLower
                 )
import Data.List ( delete
                 , elemIndex
                 , intersect
                 , isInfixOf
                 , isPrefixOf
                 )
import Data.Maybe (fromMaybe)
import Data.String ( IsString ()
                   , fromString
                   )
import Data.Version (showVersion)

import Control.Monad (unless)

import System.IO
import System.Process (callProcess)
import System.FilePath ( (</>)
                       , takeDirectory
                       )
import System.IO.Unsafe (unsafeDupablePerformIO)
import System.Directory ( doesFileExist
                        , doesDirectoryExist
                        , createDirectoryIfMissing
                        )
import System.Environment ( getEnv
                          , getArgs
                          )

import Paths_todo (version)

getEnv' :: String -> String
getEnv' = unsafeDupablePerformIO . getEnv

(+:) :: IsString a => String -> String -> a
a +: b = fromString $ a <> [' '] <> b

data Options = Editor | TodoFile | ConfigDir

instance Show Options where
    show Editor = getEnv' "EDITOR"
    show TodoFile = show ConfigDir </> "todo"
    show ConfigDir = getEnv' "XDG_CONFIG_HOME" </> "todo"

data Modes = Todo String String | Done String String | WontDo String String | Urgent' String String

instance Show Modes where
    show (Todo x y) = "\ESC[1;94m" <> x <> "\ESC[0m" <> y
    show (Done x y) = "\ESC[0;32m" <> x +: "\ESC[0m\ESC[9;90m" <> y <> "\ESC[0m"
    show (WontDo x y) = "\ESC[1;31m" <> x +: "\ESC[0m\ESC[9;91m" <> y <> "\ESC[0m"
    show (Urgent' x y) = "\ESC[1;33m" <> x <> "\ESC[0m\ESC[1;15m" <> y <> "\ESC[0m"

newtype Urgent = Urgent { sym :: String } deriving (Eq, Read, Show)

regular, urgent :: Urgent
regular = Urgent { sym = "*" }
urgent = Urgent { sym = ">" }

editor, confDir, todoFile :: String
editor = show Editor
confDir = show ConfigDir
todoFile = show TodoFile

edit, help, printV :: IO ()
edit = callProcess editor [todoFile]
printV = putStr $ unlines ["Welcome to a random terminal todo program.", "Current version:" +: showVersion version]
help = putStr $ unlines [ "Usage: todo [OPTION]..."
                        , "Manage your todo list with ease."
                        , "   --help, help, ?, -h                                 Display this and exit."
                        , "   edit, -e                                            Open the todo file with your editor set by $EDITOR."
                        , "   list, -l                                            List all entries in your todo."
                        , "   search, -s [ARGUMENTS]                              Searches the todo for the given query."
                        , "   add, -a [-u] [ARGUMENTS]                            Add [ARGUMENTS] as an entry in your todo; -u marks the entry as urgent."
                        , "   mark, -m [(d)one, (t)odo, (w)ontdo, (u)rgent] [INT] Mark the chosen entry as either 'done', 'todo', 'won't do' or 'urgent depending on the first argument."
                        , "   remove, rm, -r [INT]                                Remove the chosen entry from the todo."
                        , "   version, -v                                         Display the version of the program."
                        , "If you spot any issues with the program, please make sure to report them at https://codeberg.org/exorcist/todo"
                        ]

bootstrap :: IO ()
bootstrap = do
    x <- doesFileExist todoFile
    f <- doesDirectoryExist confDir

    unless (f && x) $ do
        createDirectoryIfMissing True $ takeDirectory todoFile
        writeFile todoFile ""
        putStrLn $ "Created" +: confDir +: "and" +: todoFile <> "."

colorize :: String -> String
colorize (x:xs) | x == '*' = show $ Todo "*" xs
                | x == '>' = show $ Urgent' ">" xs
                | x == '-' = show $ Done "-" (drop 1 xs)
                | x == '!' = show $ WontDo "!" (drop 1 xs)
                | otherwise = x:xs
colorize [] = []

readTodo :: IO String
readTodo = unlines . (colorize <$>) . lines <$> readFile todoFile

counter :: String -> [Int]
counter x = zipWith const [1..] ((filter (`elem` "*-!>") <$>) $ lines x)

list :: IO ()
list = readTodo >>= \contents ->
    if null contents
       then putStrLn "There are no tasks in your todo."
       else mapM_ putStrLn $ zipWith (+:) (show <$> counter contents) $ lines contents

add :: Urgent -> [String] -> IO ()
add (Urgent { sym = f }) xs = do
    content <- readFile todoFile

    if unwords xs == "" || unwords xs == " "
       then putStrLn "Input is too short."
       else
           case length $ filter (== unwords xs) (drop 2 <$> lines content) of
             0 -> do
                 appendFile todoFile ("\n" <> f +: unwords xs)
                 if (Urgent { sym = f }) == urgent
                    then putStrLn $ unwords xs +: "-- Added as Urgent."
                    else putStrLn $ unwords xs +: "-- Added."
             _ -> putStrLn "You already have that in the todo."

clean :: IO ()
clean = do
    handle <- openFile todoFile ReadMode
    contents <- lines <$> hGetContents' handle

    let done, wontDo :: [(Bool, String)]
        done = zip (("-" `isPrefixOf`) <$> contents) contents
        wontDo = zip (("!" `isPrefixOf`) <$> contents) contents

        toClean :: [(Bool, String)]
        toClean = done `intersect` wontDo

        cleaned :: [String]
        cleaned = snd <$> filter (not . fst) toClean

    if done == wontDo
       then putStrLn "Nothing to clean-up."
       else do
           writeFile todoFile $ unlines cleaned
           putStrLn $ "Cleaned" +: show (length contents - length cleaned) +: "entries."

    hClose handle

search :: [String] -> IO ()
search xs = do
    handle <- openFile todoFile ReadMode
    contents <- lines <$> hGetContents' handle

    let filterS :: String -> [String] -> Int
        filterS y cts = succ . length $ takeWhile (/= "True") $ show <$> map ((y `isInfixOf`) . map toLower) cts

        query :: String
        query = toLower <$> unwords xs

        search' :: String -> [String] -> Int -> IO ()
        search' qr ls n | null qr || null ls = pure ()
                        | filterS qr ls > length ls = putStrLn "No (more) matches found for this query."
                        | otherwise = do
                            putStrLn $ concat  [ "Found match on line: ["
                                               , show (filterS qr ls + n)
                                               , "]: "
                                               , drop 2 (ls !! (filterS qr ls - 1))
                                               ]
                            let deleted = delete (ls !! (filterS qr ls - 1)) ls
                            search' qr deleted (n + 1)

    search' query contents 0

    hClose handle

summary :: IO ()
summary = do
    handle <- openFile todoFile ReadMode
    contents <- lines <$> hGetContents' handle

    let filterC :: String -> String
        filterC x = show . length $ filter (== "True") $ show <$> map (x `isPrefixOf`) contents

        td, dn, wd, ur :: String
        td = show $ (read (filterC "*") :: Int) + read ur
        dn = filterC "-"
        wd = filterC "!"
        ur = filterC ">"

    mapM_ putStr [ "\n"
                 , show (Todo "* " td), " todo (" <> show (Urgent' "> " ur) <> " of which urgent); "
                 , show (Done "-" ""), dn, " done; "
                 , show (WontDo "!" ""), wd, " won't do"
                 , "\n"
                 ]

    hClose handle

insertAt :: Int -> a -> [a] -> [a]
insertAt _ n [] = [n]
insertAt i n (a:as) | i <= 0 = n:a:as
                    | otherwise = a : insertAt (i - 1) n as

takeOne :: [a] -> Int -> a
takeOne xs = (xs !!) . pred

shift :: Int -> Int
shift n | length (show n) == 1 = n + 3
        | not (null (show n)) && length (show n) > 1 = n + 2
        | otherwise = n

times :: [String] -> [String]
times xs = zipWith (+:) (show <$> counter (unlines xs)) xs

len, argToInt :: [String] -> String -> String
len f x = drop (shift . length $ takeWhile (not . isSpace) (argToInt f x)) $ argToInt f x
argToInt f = takeOne (times f) . read

action :: String -> String -> IO ()
action x g = do
    handle <- openFile todoFile ReadMode
    contents <- lines <$> hGetContents' handle

    let ds, ts, ws, us :: String
        ds = "-" +: len contents g
        ts = "*" +: len contents g
        ws = "!" +: len contents g
        us = ">" +: len contents g

        notify :: String -> IO ()
        notify = putStrLn . (len contents g +:)

        formatString :: String -> String -> String
        formatString f h = unlines . insertAt (fromMaybe 0 $ elemIndex h contents) f $ delete h contents

        lengthF :: String -> [String] -> Int
        lengthF s = length . filter (== s)

        done, todo, wontdo, remove, urgent' :: IO ()
        done = case (lengthF ds contents, lengthF ws contents, lengthF us contents) of
                 (0, 0, 0) -> do
                     writeFile todoFile $ formatString ds ts
                     notify "-- Marked as done."
                 (0, 1, 0) -> do
                     writeFile todoFile $ formatString ds ws
                     notify "-- Marked as done."
                 (0, 0, 1) -> do
                     writeFile todoFile $ formatString ds us
                     notify "-- Marked as done."
                 (_, _, _) -> putStrLn "You already have that marked as done."
        todo = case (lengthF ts contents, lengthF ws contents, lengthF us contents) of
                 (0, 0, 0) -> do
                     writeFile todoFile $ formatString ts ds
                     notify "-- Marked as todo."
                 (0, 1, 0) -> do
                     writeFile todoFile $ formatString ts ws
                     notify "-- Marked as todo."
                 (0, 0, 1) -> do
                     writeFile todoFile $ formatString ts us
                     notify "-- Marked as todo."
                 (_, _, _) -> putStrLn "You already have that marked as todo."
        wontdo = case (lengthF ws contents, lengthF ts contents, lengthF us contents) of
                   (0, 0, 0) -> do
                       writeFile todoFile $ formatString ws ds
                       notify "-- Marked as won't do."
                   (0, 1, 0) -> do
                       writeFile todoFile $ formatString ws ts
                       notify "-- Marked as won't do."
                   (0, 0, 1) -> do
                       writeFile todoFile $ formatString ws us
                       notify "-- Marked as won't do."
                   (_, _, _) -> putStrLn "You already have that marked as won't do."
        remove = case (lengthF ds contents, lengthF ws contents, lengthF us contents) of
                   (0, 0, 0) -> do
                       writeFile todoFile . unlines $ delete ts contents
                       notify "-- Removed."
                   (0, 1, 0) -> do
                       writeFile todoFile . unlines $ delete ws contents
                       notify "-- Removed."
                   (0, 0, 1) -> do
                       writeFile todoFile . unlines $ delete us contents
                       notify "-- Removed."
                   (_, _, _) -> do
                       writeFile todoFile . unlines $ delete ds contents
                       notify "-- Removed."
        urgent' = case (lengthF ws contents, lengthF ts contents, lengthF ds contents) of
                   (1, 0, 0) -> do
                       writeFile todoFile $ formatString us ws
                       notify "-- Marked as urgent."
                   (0, 1, 0) -> do
                       writeFile todoFile $ formatString us ts
                       notify "-- Marked as urgent."
                   (0, 0, 1) -> do
                       writeFile todoFile $ formatString us ds
                       notify "-- Marked as urgent."
                   (_, _, _) -> putStrLn "You already have that marked as urgent."

    if | x == "done" || x == "d" -> done
       | x == "todo" || x == "t" -> todo
       | x == "wontdo" || x == "w" -> wontdo
       | x == "remove" || x == "r" -> remove
       | x == "urgent" || x == "u" -> urgent'
       | otherwise -> error $ unlines ["Unknown flag:" +: x <> ".", "Run 'todo -h' to display all available commands."]

    hClose handle

main :: IO ()
main = do
    bootstrap
    getArgs >>= \case
        [] -> do
            list
            summary
        [[]] -> do
            list
            summary
        x:f@(y:xs) -> if | null x -> pure ()
                         | x == "add" || x == "-a" -> if y == "-u"
                                                         then add urgent xs
                                                         else add regular f
                         | x == "mark" || x == "-m" -> if length xs > 1
                                                       then mapM_ (action y) xs
                                                       else action y (takeOne xs 1)
                         | x == "remove" || x == "rm" || x == "-r" -> if length xs > 1
                                                                      then mapM_ (action "remove") f
                                                                      else action "remove" y
                         | x == "search" || x == "-s" -> search f
                         | otherwise -> error $ unlines ["Unknown command:" +: x <> ".", "Run 'todo -h' to display all available commands."]
        x:_ -> if | null x -> pure ()
                  | x == "edit" || x == "-e" -> edit
                  | x == "list" || x == "-l" -> do
                      list
                      summary
                  | x == "clean" || x == "-c" -> clean
                  | x == "version" || x == "-v" -> printV
                  | x == "--help" || x == "-h" || x == "help" || x == "?" -> help
                  | otherwise -> error $ unlines ["Unknown command:" +: x <> ".", "Run 'todo -h' to display all available commands."]
